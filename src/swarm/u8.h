/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef U8_H_SWARM
#define U8_H_SWARM

#ifndef INVALID_UTF8
# define INVALID_UTF8 -2
#endif

#include <uchar.h>

/* get_ucodep_u8c() extracts a Unicode code point from a utf8 character.
 * get_ucodep_u8c() returns a code point or INVALID_UTF8 on error.
 */
char32_t get_ucodep_u8c(char32_t u8c);

char32_t get_u8c_ucodep(char32_t ucodep);


#endif
