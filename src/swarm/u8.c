/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "u8.h"

/* get_ucodep_u8c() extracts a Unicode code point from a utf8 character.
 * On error, get_ucodep_u8c() returns INVALID_UTF8.
 */
char32_t
get_ucodep_u8c(char32_t u8c)
{
  char32_t ucodep = INVALID_UTF8;
  char32_t t[3] = {};
  size_t tail = 0;

  do {
    if ((u8c & 0xffffff80) == 0) { /* 1byte */
      ucodep = u8c;
      break;
    } else if ((u8c & (0xC080)) == 0xC080) { /* 2bytes */
      tail = 1;
      t[0] = u8c;

      /* reset unnecessary bits */
      u8c = u8c & 0x3f00;
      t[0] = t[0] & 0x7f;
    } else if ((u8c & (0xE08080)) == 0xE08080) { /* 3bytes */
      tail = 2;
      t[0] = t[1] = u8c;

      /* reset unnecessary bits */
      u8c = u8c & 0x1f0000;
      t[0] = t[0] & 0x7f00;
      t[1] = t[1] & 0x7f;
    } else if ((u8c & (0xF0808080)) == 0xF0808080) { /* 4bytes */
      tail = 3;
      t[0] = t[1] = t[2] = u8c;

      /* reset unnecessary bits */
      u8c = u8c & 0x0f000000;
      t[0] = t[0] & 0x7f0000;
      t[1] = t[1] & 0x7f00;
      t[2] = t[2] & 0x7f;
    } else {
      break;
    }

    for (size_t i=0; i < tail; i++) {
      u8c = u8c >> 2;
      u8c = u8c | t[i];
    }
    ucodep = u8c;
  } while (0);

  return ucodep;
}

char32_t
get_u8c_ucodep(char32_t ucodep)
{
  char32_t u8c = 0;

  /* code point, utf8:
   *   0x00000000 - 0x0000007f, 1 byte (ascii)
   *   0x00000080 - 0x000007ff, 2 bytes
   *   0x00000800 - 0x0000ffff, 3 bytes
   *   0x00010000 - 0x0010ffff, 4 bytes
   */
  
  do {
    if ((ucodep & 0xffffff80) == 0) { /* 1byte (ascii)*/
      u8c = ucodep;
      break;
    } else if ((ucodep >= 0x80) && (ucodep <= 0x07ff)) { /* 2 bytes */
      u8c = u8c | 0xc080;
      
      u8c = u8c | (ucodep & 0x003f);
      u8c = u8c | ((ucodep << 2) & 0xff00);
      break;
    } else if ((ucodep >= 0x0800) && (ucodep <= 0xffff)) { /* 3 bytes */
      u8c = u8c | 0xe08080;

      u8c = u8c | (ucodep & 0x3f);
      u8c = u8c | ((ucodep << 2) & 0x3f00);
      u8c = u8c | ((ucodep << 4) & 0xff0000);
      break;
    } else if ((ucodep >= 0x010000) && (ucodep <= 0x10ffff)) { /* 4 bytes */
      u8c = u8c | 0xF0808080;

      u8c = u8c | (ucodep & 0x3f);
      u8c = u8c | ((ucodep << 2) & 0x3f00);
      u8c = u8c | ((ucodep << 4) & 0x3f0000);
      u8c = u8c | ((ucodep << 6) & 0xff000000);
      break;
    } else {
      u8c = INVALID_UTF8;
      break;
    }
  } while(0);

  return u8c;
}
