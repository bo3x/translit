/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>     /* fprintf() fopen() fclose() */
#include <stdlib.h>    /* exit() */
#include <unistd.h>    /* getopt() */
#include <errno.h>
#include <string.h>    /* strcmp() strerror() */
#include <stdbool.h>
#include <ctype.h>
#include "swarm/io.h"
#include "jarolit.h"

typedef enum {jarolit_basic, jarolit_adv} transl_sys;
static void usage(void);
static void transout(FILE *fin, FILE *fout, transl_sys translit);
static void cyrout(FILE *fin, FILE *fout, transl_sys translit);
static void out_tr(FILE *fin, FILE *fout, void (*ptr)(char32_t, FILE*));
static void out_cyr(FILE *fin, FILE *fout, void (*ptr)(char32_t, FILE*, FILE*));

int
main(int argc, char **argv)
{
  FILE *fin = NULL;
  FILE *fout = stdout;
  bool tocyr = false;
  transl_sys translit = jarolit_basic;
  int o = 0;

  while ((o = getopt(argc, argv, "ro:t:h")) != -1) {
    switch (o) {
    case 'r':
      tocyr = true;
      break;
    case 't':
      if (!(strcmp(optarg, "jarolit_basic"))) {
	;
      } else if (!(strcmp(optarg, "jarolit_adv"))) {
	translit = jarolit_adv;
      } else {
	fprintf(stderr, "ERROR: %s: unknown transliteration system\n", optarg);
	exit(1);
      }
      break;
    case 'o':
      if ((fout = fopen(optarg, "w+")) == NULL) {
	fprintf(stderr, "ERROR: %s: %s\n", argv[optind-1], strerror(errno));
	exit(1);
      }
      break;
    case 'h':
      usage();
      exit(0);
      break;
    case '?':
      usage();
      exit(2);
      break;
    }
  }

  if (argv[optind] == NULL || !strcmp(argv[optind], "-")) {
    fin = stdin;
    if (tocyr) {
      cyrout(fin, fout, translit);
    } else {
      transout(fin, fout, translit);
    }
  } else {
    for (size_t i=0; argv[optind + i] != NULL; i++) {
      if ((fin = fopen(argv[optind + i], "r")) == NULL) {
	fprintf(stderr, "ERROR: %s: %s\n", argv[optind + i], strerror(errno));
	exit(1);
      }
      if (tocyr) {
	cyrout(fin, fout, translit);
      } else {
	transout(fin, fout, translit);
      }
    }
  }
  fclose(fin);
  fclose(fout);
  return 0;
}

void
transout(FILE *fin, FILE *fout, transl_sys translit)
{
  switch (translit)
    {
    case jarolit_basic:
      out_tr(fin, fout, &jarbasic_trout);
      break;
      
    case jarolit_adv:
      out_tr(fin, fout, &jaradv_trout);
      break;

    default:
      break;
    }
}

void
cyrout(FILE *fin, FILE *fout, transl_sys translit)
{
  switch (translit)
    {
    case jarolit_basic:
      out_cyr(fin, fout, &jarbasic_cyrout);
      break;
      
    case jarolit_adv:
      out_cyr(fin, fout, &jaradv_cyrout);
      break;

    default:
      break;
    }
}

void
out_tr(FILE *fin, FILE *fout, void (*ptr)(char32_t, FILE*))
{
  char32_t u8c = 0;         /* utf8 character, not codepoint */
  
  while ((u8c = fget_u8c(fin)) != (char32_t)END_OF_UTF8) {
    (*ptr)(u8c, fout);
  }
}

void
out_cyr(FILE *fin, FILE *fout, void (*ptr)(char32_t, FILE*, FILE*))
{
  char32_t u8c = 0;         /* utf8 character, not codepoint */
  
  while ((u8c = fget_u8c(fin)) != (char32_t)END_OF_UTF8) {
    (*ptr)(u8c, fin, fout);
  }
}

void
usage(void)
{
  fputs("USAGE:\n\
\ttranslit [option]... [inputfile]...\n\
\nDESCRIPTION:\n\
\ttranslit transliterates text written in the Cyrillic alphabet to text written\
 in the Latin alphabet and vice versa.\n\
\tIf no input files are given, or if it is given as a dash (-),\
 translit reads from standart input. If no output file is given,\
 translit writes to standard output.\n\
\nOPTIONS:\n\
\t-r,\tconvert Translit Text to Cyrillic Text.\n\n\
\t-o <outputfile>,\n\t\tuse outputfile for output.\n\n\
\t-t <translitname>,\n\t\tset transliteration system\
 (jarolit_basic by default).\n\n\
\t-h,\tprint this help message and exit.\n\
\nLIST OF SUPPORTED TRANSLITERATION SYSTEMS:\n\
\tjarolit_basic (non-reversible): converts cyrillic or trans-cyrillic characters only.\
 Other characters are displayed as is.\n\
\tjarolit_adv (reversible): pure ascii in output.\n\
\nBUGS & FEATURES:\n\
\tutf8 only\n", stdout);
}

