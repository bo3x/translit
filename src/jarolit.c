/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>  /* fprintf() fputc() fflush() */
#include <stdlib.h> /* abort() */
#include <string.h> /* strcpy() */
#include <ctype.h>  /* toupper() tolower() */
#include "jarolit.h"
#include "swarm/io.h"  /* fget_u8c() fput_u8c() */
#include "swarm/u8.h"  /* get_ucodep_u8c() get_u8c_ucodep() */
#include "types.h" /* translit_st */

#define MAX_LETTER_SIZE 4
#define MAX_UNICODEPOINT 0x10ffff /* max codepoint in Unicode is (0x10ffff == 1,114,111) */
#define MAX_XUCPDIG 7
#define CYR_CAPITAL_JO 0x401
#define CYR_CAPITAL_SH 0x428
#define CYR_SMALL_SH 0x448
#define CYR_CAPITAL_SHH 0x429
#define CYR_SMALL_SHH 0x449

static void normjar_cyrout(char32_t ch, FILE *fout);
static bool is_normjar(char32_t jl);
static bool is_hjar(char32_t jl);
static bool is_jjar(char32_t jl);

/* ja, jj, ju, jq, jo */
static const char32_t j_cyr[] = {'a', 0x44f, 'j', 0x439, 'u', 0x44e, 'q', 0x44a, 'o', 0x451, 0};
/* ch eh kh zh */
static const char32_t h_cyr[] = {'c', 0x447, 'e', 0x44d, 'k', 0x445, 'z', 0x436, 0};
static const char32_t norm_cyr[] = {'a', 0x430, 'b', 0x431, 'c', 0x446, 'd', 0x434, 'e', 0x435,
				    'f', 0x444, 'g', 0x433, 'i', 0x438, 'k', 0x43a, 'l', 0x43b,
				    'm', 0x43c, 'n', 0x43d, 'o', 0x43e, 'p', 0x43f, 'q', 0x44c,
				    'r', 0x440, 's', 0x441, 't', 0x442, 'u', 0x443, 'v', 0x432,
				    'y', 0x44b, 'z', 0x437, 0};

const translit_st jarolit_table[] = {
  {.ucodep=0x401, .lit="JO"},
  {.ucodep=0x410, .lit="A"},
  {.ucodep=0x411, .lit="B"},
  {.ucodep=0x412, .lit="V"},
  {.ucodep=0x413, .lit="G"},
  {.ucodep=0x414, .lit="D"},
  {.ucodep=0x415, .lit="E"},
  {.ucodep=0x416, .lit="ZH"},
  {.ucodep=0x417, .lit="Z"},
  {.ucodep=0x418, .lit="I"},
  {.ucodep=0x419, .lit="JJ"},
  {.ucodep=0x41A, .lit="K"},
  {.ucodep=0x41B, .lit="L"},
  {.ucodep=0x41C, .lit="M"},
  {.ucodep=0x41D, .lit="N"},
  {.ucodep=0x41E, .lit="O"},
  {.ucodep=0x41F, .lit="P"},
  {.ucodep=0x420, .lit="R"},
  {.ucodep=0x421, .lit="S"},
  {.ucodep=0x422, .lit="T"},
  {.ucodep=0x423, .lit="U"},
  {.ucodep=0x424, .lit="F"},
  {.ucodep=0x425, .lit="KH"},
  {.ucodep=0x426, .lit="C"},
  {.ucodep=0x427, .lit="CH"},
  {.ucodep=0x428, .lit="SH"},
  {.ucodep=0x429, .lit="SHH"},
  {.ucodep=0x42A, .lit="JQ"},
  {.ucodep=0x42B, .lit="Y"},
  {.ucodep=0x42C, .lit="Q"},
  {.ucodep=0x42D, .lit="EH"},
  {.ucodep=0x42E, .lit="JU"},
  {.ucodep=0x42F, .lit="JA"},
  
  {.ucodep=0x430, .lit="a"},
  {.ucodep=0x431, .lit="b"},
  {.ucodep=0x432, .lit="v"},
  {.ucodep=0x433, .lit="g"},
  {.ucodep=0x434, .lit="d"},
  {.ucodep=0x435, .lit="e"},
  {.ucodep=0x436, .lit="zh"},
  {.ucodep=0x437, .lit="z"},
  {.ucodep=0x438, .lit="i"},
  {.ucodep=0x439, .lit="jj"},
  {.ucodep=0x43A, .lit="k"},
  {.ucodep=0x43B, .lit="l"},
  {.ucodep=0x43C, .lit="m"},
  {.ucodep=0x43D, .lit="n"},
  {.ucodep=0x43E, .lit="o"},
  {.ucodep=0x43F, .lit="p"},
  {.ucodep=0x440, .lit="r"},
  {.ucodep=0x441, .lit="s"},
  {.ucodep=0x442, .lit="t"},
  {.ucodep=0x443, .lit="u"},
  {.ucodep=0x444, .lit="f"},
  {.ucodep=0x445, .lit="kh"},
  {.ucodep=0x446, .lit="c"},
  {.ucodep=0x447, .lit="ch"},
  {.ucodep=0x448, .lit="sh"},
  {.ucodep=0x449, .lit="shh"},
  {.ucodep=0x44A, .lit="jq"},
  {.ucodep=0x44B, .lit="y"},
  {.ucodep=0x44C, .lit="q"},
  {.ucodep=0x44D, .lit="eh"},
  {.ucodep=0x44E, .lit="ju"},
  {.ucodep=0x44F, .lit="ja"},
  {.ucodep=0x451, .lit="jo"},
  {.ucodep=-1, .lit=0}};

bool
get_jarletter(char32_t ucodep, char *jar_letter)
{
  bool flag = false;
  size_t len_jarletter=sizeof(jar_letter);
  
  for (size_t i=0; jarolit_table[i].ucodep != -1; i++) {
    if (jarolit_table[i].ucodep == ucodep) {
      if (len_jarletter >= sizeof(jarolit_table[i].lit)) {
	strcpy(jar_letter, jarolit_table[i].lit);
	flag = true;
	break;
      } else {
	fprintf(stderr,
		"ERROR: jarolit.c: get_jarletter(): insufficient buffer size (%lu)\n",
		len_jarletter);
	abort();
      }
    }
  }
  return flag;
}

void
jarbasic_trout(char32_t u8c, FILE *fout)
{
  char jar_letter[MAX_LETTER_SIZE] = {};

  if (get_jarletter(get_ucodep_u8c(u8c), jar_letter)) {
    fputs(jar_letter, fout); /* ascii out */
  } else {
    fput_u8c(u8c, fout);     /* utf8 out */
  }
}

void
jaradv_trout(char32_t u8c, FILE *fout)
{
  static bool xe_flag = false;
  static bool xr_flag = false;
  char32_t ucodep = get_ucodep_u8c(u8c);
  char jar_letter[MAX_LETTER_SIZE] = {};
  size_t n = 1;
  enum {ascii, ascii_latin, cyr, others} state;
  /* 
   * LatinText    ===> xe<LatinText> x = xx, j = jj, w = ww
   * CyrrilicText ===> xr<TranslitText>
   * utf8 char    ===> xu<CODEPOINT>x
   */
  if (((u8c >= 0x41) && (u8c <= 0x5a)) ||
      ((u8c >= 0x61) && (u8c <= 0x7a))) {
    state = ascii_latin;
  } else if (u8c <= 0x7f) {
    state = ascii;
  } else if (((ucodep >= 0x410) && (ucodep <= 0x44f)) ||
	     (ucodep == 0x401) || (ucodep == 0x451)) {
    state = cyr;
  } else {
    state = others;
  }
  
  switch (state) {
  case ascii:
    fputc(u8c, fout);
    break;
    
  case ascii_latin:
    xr_flag = false;
    if (!(xe_flag)) {
      fputs("xe", fout);
      xe_flag = true;
    }

    if (tolower(u8c) == 'x' || tolower(u8c) == 'j' || tolower(u8c) == 'w') {
      n=2;
    }
    for (size_t i=0; i < n; ++i) {
      fputc(u8c, fout);
    }
    break;
    
  case cyr:
    if (!(xr_flag) && xe_flag) {
      fputs("xr", fout);
      xr_flag = true;
    }
    xe_flag = false;
    
    if (get_jarletter(get_ucodep_u8c(u8c), jar_letter)) {
      fputs(jar_letter, fout); /* ascii out */
    }
    break;
    
  case others:
    xe_flag = false;
    xr_flag = false;
    fprintf(fout, "xu%xx", get_ucodep_u8c(u8c));
    break;
  } /* end switch(state) */
}


/* ============================================================ */
void
jarbasic_cyrout(char32_t u8c, FILE *fin, FILE *fout)
{
  static char32_t t_u8c = 0;
  static char32_t prev_lit = 0;
  static size_t h_cnt = 0;
  static bool shh_flag_global = false;
  static enum {norm_lit, j_lit, h_lit} state = norm_lit;

  switch (state)
    {
    case norm_lit:
      t_u8c = fgetc(fin); /* ascii only */
      ungetc(t_u8c, fin);
      if (tolower(u8c) == 'j') {
	prev_lit = u8c;
	state = j_lit;
      } else if ((tolower(t_u8c) == 'h') ||
		 (tolower(u8c) == 'h')) {
	prev_lit = u8c;
	state = h_lit;
      } else if (is_normjar(u8c)) {
	normjar_cyrout(u8c, fout);
      } else {
	fput_u8c(u8c, fout);
      }
      break;

    case j_lit:
      if (prev_lit == 'j' && is_jjar(u8c)) {
	for (size_t i=0; j_cyr[i] != 0; i += 2) {
	  if (tolower(u8c) == j_cyr[i]) {
	    fput_u8c(get_u8c_ucodep(j_cyr[i+1]), fout);
	  }
	}
      } else if (prev_lit == 'J' && is_jjar(u8c)) {
	for (size_t i=0; j_cyr[i] != 0; i += 2) {
	  if (u8c == 'O' || u8c == 'o') {
	    fput_u8c(get_u8c_ucodep(CYR_CAPITAL_JO), fout); /* 0x401 JO */
	    break;
	  } else if (u8c == toupper(j_cyr[i]) || u8c == tolower(j_cyr[i])) {
	    fput_u8c(get_u8c_ucodep(j_cyr[i+1] - 0x20), fout);
	    break;
	  }
	}
      } else {
	fput_u8c(prev_lit, fout);
	fput_u8c(u8c, fout);
      }
      state = norm_lit;
      break;

    case h_lit:
      ++h_cnt;
      if (tolower((t_u8c = fgetc(fin))) != 'h') {
	state = norm_lit;
      }
      ungetc(t_u8c, fin);
      switch (h_cnt)
	{
	case 1:
	  if ((state == h_lit) && (tolower(prev_lit) == 's')) {
	    shh_flag_global = true;
	  } else if ((state == norm_lit) && is_hjar(prev_lit)) {
	    for (size_t i=0; h_cyr[i] != 0; i += 2) {
	      if (prev_lit == 's') {
		fput_u8c(get_u8c_ucodep(CYR_SMALL_SH), fout); /* 0x448 sh */
		h_cnt=0;
		break;
	      } else if (prev_lit == 'S') {
		fput_u8c(get_u8c_ucodep(CYR_CAPITAL_SH), fout); /* 0x428 SH */
		h_cnt=0;
		break;
	      } else if (prev_lit == h_cyr[i]) { /* ch eh kh zh */
		fput_u8c(get_u8c_ucodep(h_cyr[i+1]), fout);
		h_cnt = 0;
		break;
	      } else if (prev_lit == toupper(h_cyr[i])) { /* CH EH KH ZH */
		fput_u8c(get_u8c_ucodep(h_cyr[i+1] - 0x20), fout);
		h_cnt = 0;
		break;
	      }
	    }
	  } else {
	    if (state != h_lit) {
	      h_cnt = 0;
	    }
	    if (is_normjar(prev_lit)) {
	      normjar_cyrout(prev_lit, fout);
	    } else {
	      fput_u8c(prev_lit, fout);
	    }
	    fput_u8c(u8c, fout);
	  }
	  break;

	case 2:
	  if (state != h_lit) {
	    h_cnt = 0;
	  }
	  if (shh_flag_global && (prev_lit == 's')) {
	    fput_u8c(get_u8c_ucodep(CYR_SMALL_SHH), fout); /* 0x449 shh */
	    shh_flag_global = false;
	  } else if (shh_flag_global && (prev_lit == 'S')) {
	    fput_u8c(get_u8c_ucodep(CYR_CAPITAL_SHH), fout); /* 0x429 SHH */
	    shh_flag_global = false;
	  } else {
	    fput_u8c(u8c, fout);
	  }
	  break;

	default:
	  if (state != h_lit) {
	    h_cnt = 0;
	  }
	  fput_u8c(u8c, fout);
	  break;
	} /* end switch(h_cnt) */
    } /* end switch(state) */
}

void
jaradv_cyrout(char32_t u8c, FILE *fin, FILE *fout)
{
  static enum {norm_lit, j_lit, h_lit, x_lit} state = norm_lit;
  char cur_ch = 0;
  char tmp_ch = 0;
  static char prev_ch = 0;

  /* h_lit */
  static size_t h_cnt = 0;
  static bool shh_flag = false;

  /* x_lit */
  static bool xe_flag=false;
  static bool xu_flag=false;
  static bool xu_close_flag=false;
  static char codepoint[MAX_XUCPDIG] = {};
  static size_t cur_cell_codepoint = 0;
  long int cp_dig = 0;

  /* ============================================================ */
  /* Input text is ascii text. other characters are ignored.
   * u8c -- must be always ascii.
   */
  if (u8c <= 0x7f) {
    cur_ch = (char)u8c;
  } else {
    return;
  }

  /*
   * j_lit:
   *    xu<CODEPOINT>x ===> utf8 char
   * x_lit:
   *    xe<LatinText>, <xx> = x, <jj> = j, <ww> = w ===> latin text
   *    xr<TranslitText> ===> cyrillic text
   */
  switch (state)
    {
    case norm_lit:
      tmp_ch = fgetc(fin);
      ungetc(tmp_ch, fin);
      if (tolower(cur_ch) == 'j') {          /* j_state */
	prev_ch = cur_ch;
	state = j_lit;
      } else if ((tolower(tmp_ch) == 'h') || /* h_state */
		 (tolower(cur_ch) == 'h')) {
	prev_ch = cur_ch;
	state = h_lit;
      } else if (tolower(cur_ch) == 'x') {   /* x_state */
	prev_ch = cur_ch;
	state = x_lit;
      } else if (is_normjar(cur_ch)) {         /* cyrillic */
	normjar_cyrout(cur_ch, fout);
      } else {
	fputc(cur_ch, fout);
      }
      break;

    case j_lit:
      if (prev_ch == 'j' && is_jjar(cur_ch)) { /* small cyr */
	for (size_t i=0; j_cyr[i] != 0; i += 2) {
	  if (tolower(cur_ch) == j_cyr[i]) {
	    fput_u8c(get_u8c_ucodep(j_cyr[i+1]), fout);
	  }
	}
	state = norm_lit;
      } else if (prev_ch == 'J' && is_jjar(cur_ch)) { /* capital cyr */
	for (size_t i=0; j_cyr[i] != 0; i += 2) {
	  if (cur_ch == 'O' || cur_ch == 'o') {
	    fput_u8c(get_u8c_ucodep(CYR_CAPITAL_JO), fout); /* 0x401 JO */
	    break;
	  } else if (cur_ch == toupper(j_cyr[i]) || cur_ch == tolower(j_cyr[i])) {
	    fput_u8c(get_u8c_ucodep(j_cyr[i+1] - 0x20), fout);
	    break;
	  }
	}
	state = norm_lit;
      } else {
	fflush(fout);
	fprintf(stderr, "\nERROR: jarolit.c: jaradv_cyrout(): not valid jarolit symbol\n");
	exit(1);
      }
      break;

    case h_lit:
      ++h_cnt;
      if (tolower((tmp_ch = fgetc(fin))) != 'h') {
	state = norm_lit;
      }
      ungetc(tmp_ch, fin);
      switch (h_cnt)
	{
	case 1:
	  if ((state == h_lit) && (tolower(prev_ch) == 's')) {
	    shh_flag = true;
	  } else if ((state == norm_lit) && is_hjar(prev_ch)) {
	    for (size_t i=0; h_cyr[i] != 0; i += 2) {
	      if (prev_ch == 's') {
		fput_u8c(get_u8c_ucodep(CYR_SMALL_SH), fout); /* 0x448 sh */
		h_cnt=0;
		break;
	      } else if (prev_ch == 'S') {
		fput_u8c(get_u8c_ucodep(CYR_CAPITAL_SH), fout); /* 0x428 SH */
		h_cnt=0;
		break;
	      } else if (prev_ch == h_cyr[i]) { /* ch eh kh zh */
		fput_u8c(get_u8c_ucodep(h_cyr[i+1]), fout);
		h_cnt = 0;
		break;
	      } else if (prev_ch == toupper(h_cyr[i])) { /* CH EH KH ZH */
		fput_u8c(get_u8c_ucodep(h_cyr[i+1] - 0x20), fout);
		h_cnt = 0;
		break;
	      }
	    }
	  } else {
	    if (state != h_lit) {
	      h_cnt = 0;
	    }
	    if (is_normjar(prev_ch)) {
	      normjar_cyrout(prev_ch, fout);
	    } else {
	      fputc(prev_ch, fout);
	    }
	    fputc(cur_ch, fout);
	  }
	  break;

	case 2:
	  if (state != h_lit) {
	    h_cnt = 0;
	  }
	  if (shh_flag && (prev_ch == 's')) {
	    fput_u8c(get_u8c_ucodep(CYR_SMALL_SHH), fout); /* 0x449 shh */
	    shh_flag = false;
	  } else if (shh_flag && (prev_ch == 'S')) {
	    fput_u8c(get_u8c_ucodep(CYR_CAPITAL_SHH), fout); /* 0x429 SHH */
	    shh_flag = false;
	  } else {
	    fputc(cur_ch, fout);
	  }
	  break;

	default:
	  if (state != h_lit) {
	    h_cnt = 0;
	  }
	  fputc(cur_ch, fout);
	  break;
	} /* end switch(h_cnt) */

    case x_lit:
    /*
     * x_lit:
     *   xe<LatinText>, <xx> = x, <jj> = j, <ww> = w ===> latin text
     *   xr<TranslitText> ===> cyrillic text
     *   xu<CODEPOINT>x ===> utf8 char
     */

      if (tolower(prev_ch) == 'x' &&
	  tolower(cur_ch) != 'x') {
	switch (tolower(cur_ch)) {
	case 'e':
	  xe_flag = true;   /* xe mode */
	  xu_flag = false;
	  prev_ch = 0;
	  break;
	case 'u':           /* xu mode */
	  xe_flag = false;
	  xu_flag = true;
	  prev_ch = 0;
	  break;
	case 'r':           /* xr mode (norm_lit) */
	  xe_flag = false;
	  xu_flag = false;
	  prev_ch = 0;
	  state = norm_lit;
	  break;
	}
	break;
      } 

      if (xe_flag) {
	if (!(prev_ch)) {
	  if ((tolower(cur_ch) == 'x') ||
	      (tolower(cur_ch) == 'w') ||
	      (tolower(cur_ch) == 'j')) {
	    prev_ch = cur_ch;
	    break;
	  }
	}
	if (!(prev_ch) ||
	    !(prev_ch - cur_ch)) {
	  fputc(cur_ch, fout);
	  prev_ch = 0;
	  break;
	}
      } /* end if(xe_flag) */

      if (xu_flag) {
	if (xu_close_flag) {
	  cp_dig = strtol((char*)&codepoint, NULL, 16);
	  if (cp_dig <= MAX_UNICODEPOINT) {
	    fput_u8c(get_u8c_ucodep(cp_dig), fout);
	    for (size_t i=0; i<MAX_XUCPDIG; ++i) {
	      codepoint[i] = 0;
	    }
	    cur_cell_codepoint = 0;
	    xu_close_flag = false;
	    xu_flag = false;
	    state = norm_lit;
	    break;
	  } else {
	    fflush(fout);
	    fprintf(stderr, "ERROR: jarolit.c: jaradv_cyrout(): codepoint overflow\n");
	    exit(1);
	  }
	} else if (isxdigit(cur_ch)) { /**/
	  tmp_ch = fgetc(fin);
	  ungetc(tmp_ch, fin);
	  if (tolower(tmp_ch) == 'x') {
	    xu_close_flag = true;
	  }
	  if (cur_cell_codepoint < MAX_XUCPDIG) {
	    codepoint[cur_cell_codepoint] = cur_ch;
	    ++cur_cell_codepoint;
	  } else {
	    fflush(fout);
	    fprintf(stderr, "ERROR: jarolit.c: jaradv_cyrout(): codepoint_string overflow\n");
	    exit(1);
	  }
	}
      } /* end if(xu_flag) */
      break;
    } /* end switch(state) */
}

void
normjar_cyrout(char32_t ch, FILE *fout)
{
  for (size_t i=0; norm_cyr[i] != 0; i += 2) {
    if (ch == norm_cyr[i]) {
      fput_u8c(get_u8c_ucodep(norm_cyr[i+1]), fout);
      break;
    } else if (ch == toupper(norm_cyr[i])) {
      fput_u8c(get_u8c_ucodep(norm_cyr[i+1] - 0x20), fout);
    }
  }
}

bool
is_normjar(char32_t jl)
{
  bool b=false;
  static const char32_t valid_normjar[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'i',
				 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				 's', 't', 'u', 'v', 'y', 'z', 0};
  
  for (size_t i=0; valid_normjar[i] != 0; ++i) {
    if ((jl == valid_normjar[i]) ||
	(jl == toupper(valid_normjar[i]))) {
      b=true;
      break;
    }
  }
  
  return b;
}

bool
is_hjar(char32_t jl)
{
  bool b = false;
  static const char32_t valid_hjar[] = {'c', 'e', 'k', 'z', 's', 0};

  for (size_t i=0; valid_hjar[i] != 0; ++i) {
    if ((jl == valid_hjar[i]) ||
	(jl == toupper(valid_hjar[i]))) {
      b = true;
      break;
    }
  }
  
  return b;
}

bool
is_jjar(char32_t jl)
{
  bool b = false;
  static const char32_t valid_jjar[] = {'a', 'j', 'u', 'q', 'o', 0};

  for (size_t i=0; valid_jjar[i] != 0; ++i) {
    if ((jl == valid_jjar[i]) ||
	(jl == toupper(valid_jjar[i]))) {
      b = true;
      break;
    }
  }
  
  return b;
}

