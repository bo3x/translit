.PHONY: all clean install uninstall
SRC_DIR=src

all:
	@$(MAKE) -s -C $(SRC_DIR) all

clean:
	@$(MAKE) -s -C $(SRC_DIR) clean

install:
	@$(MAKE) -s -C $(SRC_DIR) install

uninstall:
	@$(MAKE) -s -C $(SRC_DIR) uninstall

