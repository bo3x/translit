# DESCRIPTION:
**_translit_** transliterates text written in the Cyrillic alphabet to text written in the Latin alphabet and vice versa.

# INSTALLATION
(build deps: make, gcc, glibc)
```
make
make install
```

# UNINSTALLATION
```
make uninstall
```

# LIST OF SUPPORTED TRANSLITERATION SYSTEMS:
* jarolit_basic (non-reversible): converts cyrillic or trans-cyrillic characters only. Other characters are displayed as is.
* jarolit_adv (reversible): pure ascii in output. ([link](https://программирование-по-русски.рф/%d1%8f%d1%80%d0%be%d0%bb%d0%b8%d1%82.%d1%8f%d1%80%d0%b3%d1%82/))

