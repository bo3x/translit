#!/usr/bin/pil

# global variables =============================================================
(argv textfile progname)
(ifn (str? progname)
   (setq progname "/usr/local/bin/translit"))

#===============================================================================
(de test_trans (Translit)
   (prinl Translit)
   (let (Tr_file (pack (dirname textfile) "trans-" (basename textfile))
         Cyr_file (pack (dirname textfile) "cyr-" (basename textfile)) )
      (let (To_trans (list progname "-t" Translit "-o" Tr_file textfile)
            To_cyr   (list progname "-r" "-t" Translit "-o" Cyr_file Tr_file)
            Diff     (list 'diff "-q" textfile Cyr_file) )
         (mapc cll (list To_trans To_cyr) (list 'To_trans 'To_cyr))
         (if (match "jarolit_adv" Translit) (cll Diff 'Diff)) ) ) )

(de cll (L Tr)
   (cond
      ((match "To_trans" Tr) (prin "  to_trans: "))
      ((match "To_cyr" Tr)   (prin "  to_cyr: "))
      ((match "Diff" Tr)     (prin "  diff: ")) )
   (apply call L)
   (if (n0 @@)
      (prinl "FAILED : " @@)
      (prinl "OK : " @@) ) )
      
(de main ()
   (cond
      ((=0 (length (argv)))
       (prinl "USAGE: test.l textfile_cyrillic_utf8 [path_to_translit]") (bye) )
      ((not (info textfile))
       (prinl (list textfile progname) ": No such file") (bye) )
      ((not (info progname))
       (prinl progname ": No such file") (bye) ) )
   
   (prinl "textfile: " textfile)
   (prinl "progname: " progname)
   (mapcar test_trans '("jarolit_basic" "jarolit_adv"))
   (bye) )
         
(main)

